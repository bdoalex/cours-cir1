<?php
  include('../model/fonction.php');
  if(isset($_POST['submit'])){
  		if(!empty($_POST['login']) AND (strlen($_POST['password']) > 0)){

        $requser = $bdd->prepare("SELECT * FROM users WHERE login = ?");
  			$requser->execute(array($_POST['login']));
        $userinfo = $requser->fetch(); 

  			if($userinfo != FALSE && password_verify($_POST['password'], $userinfo['password']) == TRUE){


          $_SESSION['id']=$userinfo['id'];
          $_SESSION['login']=$userinfo['login'];
          $_SESSION['rank']=$userinfo['rank'];
          header("Location: ../controller/calendar.php");
          }
  			else {
  				$error = "Mauvais login/password !";
  			}
      }
  		else {
        $error = "Tous les champs ne sont pas rempli !";
      }

  	}
  include('../view/login.php');

?>
