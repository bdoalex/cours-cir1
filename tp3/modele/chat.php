<?php
try {
   $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'root');
} catch (Exception $e) {
      exit('Erreur de connexion à la base de données.');
}
function envoyer($pseudo, $message, $bdd){//permet d'envoyer le message
  $insertion = $bdd->prepare('INSERT INTO messages(pseudo, message, date) VALUES(:pseudo, :message, :date)');
  $insertion->execute(array(
    'pseudo' => htmlspecialchars($pseudo),
    'message' => htmlspecialchars($message),
    'date' => date('Y-m-d')));
  $insertion->closeCursor();
}

function afficher($message, $bdd){ //permet d'afficher le message envoyé
  $explodeString = explode(" ", $message);
  if($explodeString[0] === '/me'){
    $explodeString[0] = '';
    $implodeString = implode(" ", $explodeString);
    echo '<em>' . $implodeString . '</em>';
  }
}
?>
