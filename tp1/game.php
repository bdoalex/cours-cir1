<?php
<<<<<<< HEAD

//Ici on créer la première génération
function firstGen($taille, $grille){
    for($i = 0; $i < $taille; $i++){
      for($j = 0; $j < $taille; $j++){
        $random = rand(0, 1);
        if($random == 0){
          $grille[$i][$j] = 'o';
        }
        else{
          $grille[$i][$j] = '   ';
        }
      }
    }
    return $grille;
}

//ici on affiche la grille du jeu
function printGrille($taille, $grille){
  for($i = 0; $i < $taille; $i++){
    for($j = 0; $j < $taille; $j++){
       echo $grille[$i][$j];
    }
    echo '<br>';
  }
}

//ici on compte le nombre de voisin
function voisin($ligne, $colonne, $grille){
  $voisin = 0;
  if(isset($grille[$ligne - 1][$colonne])){
    if($grille[$ligne - 1][$colonne] == 'o'){
      $voisin++;
    }
  }
  if(isset($grille[$ligne][$colonne - 1])){
    if($grille[$ligne][$colonne - 1] == 'o'){
      $voisin++;
    }
  }
  if(isset($grille[$ligne - 1][$position - 1])){
    if($grille[$ligne - 1][$colonne - 1] == 'o'){
      $voisin++;
    }
  }
  if(isset($grille[$ligne + 1][$colonne + 1])){
    if($grille[$ligne + 1][$colonne + 1] == 'o'){
      $voisin++;
    }
  }
  if(isset($grille[$ligne - 1][$colonne + 1])){
    if($grille[$ligne - 1][$colonne + 1] == 'o'){
      $voisin++;
    }
  }

  if(isset($grille[$ligne + 1][$colonne - 1])){
    if($grille[$ligne + 1][$colonne - 1] == 'o'){
      $voisin++;
    }
  }

  if(isset($grille[$ligne][$colonne + 1])){
    if($grille[$ligne][$colonne + 1] == 'o'){
      $voisin++;
    }
  }

  if(isset($grille[$ligne + 1][$colonne])){
    if($grille[$ligne + 1][$colonne] == 'o'){
      $voisin++;
    }
  }

  return $voisin;
}

//reproduction d'une nouvelle grille pour la prochain generation
function newGen($taille, $grille){
  $newGrille = array();
  for($i = 0; $i < $taille; $i++){
    $newGrille[$i] = array();
  }

//on applique les règles pour la prochaine grille
  for($i = 0; $i < $taille; $i++){
    for($j = 0; $j < $taille; $j++){
      if($grille[$i][$j] == 'o'){
        if(voisin($i, $j, $grille) == 3 || voisin($i, $j, $grille) == 2){
          $newGrille[$i][$j] = 'o';
        }
        else{
          $newGrille[$i][$j] = '   ';
        }
      }
      else{
        if(voisin($i, $j, $grille) == 3){
          $grille[$i][$j] = 'o';
        }
        else{
          $newGrille[$i][$j] = '   ';
        }
      }
    }
  }
  return $newGrille;
=======
function initializeGame($nbOfRow, $nbOfCol) {
	$game = [];
	for($i = 0; $i < $nbOfRow; $i++) {
		$game[] = [];
		for($j = 0; $j < $nbOfCol; $j++) {
			$game[$i][$j] = (bool) random_int(0, 1);
		}
	}
	return $game;
}


function countNeightboors($game, $row, $col) {
	$neigboorsCoordinates = [[$row - 1, $col - 1], [$row - 1, $col], [$row - 1, $col + 1],
	                         [$row, $col - 1], [$row, $col + 1],
	                         [$row + 1, $col - 1], [$row + 1, $col], [$row + 1, $col + 1]];
	$neighboors = 0;
	foreach($neigboorsCoordinates as $coordinates) {
		if (isset($game[$coordinates[0]]) && isset($game[$coordinates[0]][$coordinates[1]])) {
			$neighboors += $game[$coordinates[0]][$coordinates[1]];
		}
	}
	return $neighboors;
}


function birth($array, $row, $col) {
	return (int) (countNeightboors($array, $row, $col) == 3);
}

function death($array, $row, $col) {
	return (int) !(countNeightboors($array, $row, $col) >= 4 || countNeightboors($array, $row, $col) <= 1);
}

function nextGeneration($oldGeneration) {
	$newGeneration = initializeGame(count($oldGeneration), count($oldGeneration[0]));
	foreach($oldGeneration as $rowIndex => $row) {
		foreach($row as $colIndex => $value) {
			$newGeneration[$rowIndex][$colIndex] = $value ? death($oldGeneration, $rowIndex, $colIndex) : birth($oldGeneration, $rowIndex, $colIndex);
		}
	}
	return $newGeneration;
}

function gameIsStill($old, $new) {
	foreach($old as $rowIndex => $row) {
		if (is_array($row) && !gameIsStill($row, $new[$rowIndex])) {
			return false;
		} else if (!is_array($row) && $row != $new[$rowIndex]) {
			return false;
		}
	}
	return true;
>>>>>>> 272f1d8308cdeceb2dbe7f694a1e849433507674
}
